// JavaScript Document
// Action Timer Script by Brian Bochicchio 
// This timer script is used to perform an action, based on a timer tick.


var timer_is_on = 0;  // 0 means the timer is off and 1 means it's on - Treat this like a Boolean
var tick = 0; // tick is used to show how many times the timer (Timeout) has happened

function startTimer()
{
    // Update the main document's Value of Tick: text
   document.getElementById("tickOutput").innerHTML = tick;

// Below we check to see if the timer is already. If it is we do nothing. If not we start it up.

if (!timer_is_on) // The way to read this is, "if the timer is NOT on" do the things below
  {
  console.log("Starting timer");
  timer_is_on=1;
  timerCount();
  }
}

function timerCount()
{

    // set the interval of a "tick" in milliseconds and the function to call when we reach the interval
    t=setTimeout("timerCount()",1000);

    // add 1 to the tick count
    tick += 1;
    // Prints the Timer Value to the Developer Console.
	console.log(tick);
    document.getElementById("tickOutput").innerHTML = tick;

    // Checks to see if 10 seconds has passed. If so, write to the console and show the boom div.
    // If not, make sure the div is hidden
    if ( tick % 10 == 0) {
	// Writes to the console confirming it.
	console.log("BOOM!!!");
    document.getElementById("tickOutput").innerHTML = tick;
    document.getElementById("boom").style.display = "block";
    } else{
        document.getElementById("boom").style.display = "none";
}

}

function stopTimer()
{
clearTimeout(t);
timer_is_on=0;
}

function resetTimer()
{
    console.log("Resetting \'tick\'");
    tick = 0;
    document.getElementById("tickOutput").innerHTML = tick;

}
